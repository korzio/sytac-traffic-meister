const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    app: './app/index.js',
    material: './app/material.js',
  },
  output: {
    path: __dirname,
    filename: './app/[name].min.js',
    libraryTarget: 'var',
    library: 'run',
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
        },
      },
      {
        test: /\.css$/,
        loader: 'style!css',
      },
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass', 'resolve-url', 'sass?sourceMap'],
      }
    ],
  },
  sassLoader: {
    includePaths: [path.resolve(__dirname, './node_modules/materialize-css/sass/')],
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin()
  ],
};
