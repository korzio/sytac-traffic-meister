const {
  option,
  collect,
  find,
  getFormValue,
} = require('./utils');
const {
  fetchData,
} = require('../service');

/**
 * @function
 * @name run
 * @description
 * Implement all business logic - requests data from service
 * Applies transformed instances to view
 *
 * @requires trafficMeister
 * @requires form
 */
const run = (form) => {
  const summary = form.querySelector('.summary');
  let traffic;

  /**
   * @function
   * @name loadTraffic
   *
   * @description
   * Collects data for filters
   * Keeps last valuable filter state
   * Applies transformed data to selects
   *
   * @param {string} origin - changed input key
   * @param {FormValue} values
   */
  const loadTraffic = (function loadTrafficInit() {
    // keep last valuable filter state
    let valuable;
    return (event) => {
      const values = getFormValue(form);
      if (event) {
        const { target: { name: origin } } = event;
        if (origin !== 'filter') {
          valuable = origin;

          // do not update if filter absents
          if (!values.filter) {
            return;
          }
        }
      }

      const filters = values.filter && valuable ? {
        [valuable]: values[valuable],
      } : null;

      const transformed = collect(traffic, filters);

      Object.keys(transformed)
        .forEach((key) => {
          const options = transformed[key];
          const element = form[key];
          element.innerHTML =
            element.options[0].outerHTML +
            options.map(option).join('');

          const valueIndex = (options.length === 1 ? 0 : options.indexOf(values[key])) + 1;
          if (valueIndex) {
            element.selectedIndex = valueIndex;
          }
        });
    };
  }());

  /**
   * @function
   * @name showSummary
   * @description
   * Show summary information
   * @inner
   */
  const showSummary = () => {
    const selected = getFormValue(form);
    const { colors, type, brand } = selected;
    if (!(colors || type || brand)) {
      return;
    }

    const instanceFound = find(traffic, { colors, type, brand });

    summary.innerHTML = `Sorry,
      ${colors ? `<span style="color: ${colors};">${colors}</span>` : ''}
      ${type || ''}
      ${brand ? `<strong>${brand}</strong>` : ''}
      is currently not available.
      ${instanceFound ? `<br><img width="100%" src="${instanceFound.img}">` : ''}`;
  };

  /**
   * @function
   * @name showError
   * @description
   * Service error handler
   * @inner
   */
  const showError = (err) => {
    summary.innerHTML = `Sorry, service is temporarily unavailable
      <br>
      <button>Try again!</button>
      <hr>
      Error Code: ${err}`;
  };

  /**
   * @function
   * @name load
   * @description
   * Update traffic state
   * @inner
   */
  const load = () => {
    summary.innerHTML = 'Loading...';
    fetchData((err, result) => {
      if (err) {
        showError(err);
        return;
      }

      traffic = result;
      summary.innerHTML = 'Please choose vehicle\'s parameters';

      loadTraffic();
    });
  };

  /**
   * @function
   * @name bindEvents
   * @listens form:click - reload
   * @listens form:change - filter
   * @inner
   */
  const bindEvents = () => {
    form.addEventListener('click', ({ target: { tagName } }) => {
      if (tagName.toUpperCase() !== 'BUTTON') {
        return;
      }
      load();
    });
    form.addEventListener('change', (event) => {
      loadTraffic(event);
      showSummary();
    });
  };

  // start
  bindEvents();
  load();
};

module.exports = run;
