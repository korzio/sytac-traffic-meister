const {
  intercept,
  merge,
  collect,
  find,
  capitalize,
  option,
} = require('./utils');

describe('utils', () => {
  describe('intercept()', () => {
    it('exposes array with instance if it is not found in initial array', () => {
      expect(intercept(['a', 'b', 2], 1)).toEqual([1]);
    });

    it('exposes empty array if instance presents in initial array', () => {
      expect(intercept(['a', 'b', 2], 'b')).toEqual([]);
    });
  });

  describe('merge()', () => {
    it('merges two arrays into one', () => {
      expect(merge(['a', 'b', 2], [1])).toEqual(['a', 'b', 2, 1]);
    });

    it('exposes array containing unique instances', () => {
      expect(merge(['a', 'b', 2], ['b'])).toEqual(['a', 'b', 2]);
      expect(merge(['a', 'b', 2], 'b')).toEqual(['a', 'b', 2]);
    });
  });

  describe('collect()', () => {
    it('aggregates types, brands, colors from data array', () => {
      const initial = [{
        type: 'a',
        brand: 'b',
        colors: ['c'],
      }];
      const expected = {
        type: ['a'],
        brand: ['b'],
        colors: ['c'],
      };

      expect(collect(initial)).toEqual(expected);
    });

    it('aggregates data when multiple filters present', () => {
      const initial = [{
        type: 'a',
        brand: 'b',
        colors: ['c'],
      }, {
        type: 'a',
        brand: 'd',
        colors: ['e', 'c'],
      }];

      expect(collect(initial, { type: 'a' })).toEqual({
        brand: ['b', 'd'],
        colors: jasmine.arrayContaining(['e', 'c']),
      });

      expect(collect(initial, { colors: 'c' })).toEqual({
        type: ['a'],
        brand: ['b', 'd'],
      });

      expect(collect(initial, { type: 'a', brand: 'd' })).toEqual({
        colors: ['e', 'c'],
      });

      expect(collect(initial, { type: 'a', colors: 'c' })).toEqual({
        brand: ['b', 'd'],
      });
    });
  });

  describe('find()', () => {
    it('find traffic by given filters', () => {
      const initial = [{
        type: 'a',
        brand: 'b',
        colors: ['c'],
      }, {
        type: 'a',
        brand: 'd',
        colors: ['e', 'c'],
      }];

      expect(find(initial, { type: 'a' })).toEqual(initial[0]);
      expect(find(initial, { type: 'a', brand: 'd' })).toEqual(initial[1]);
      expect(find(initial, { colors: 'c' })).toEqual(initial[0]);
      expect(find(initial, { colors: 'c', brand: 'd' })).toEqual(initial[1]);
    });
  });

  describe('capitalize()', () => {
    it('transforms string to camelCase', () => {
      const initial = 'test';
      const expected = 'Test';

      expect(capitalize(initial)).toEqual(expected);
    });
  });

  describe('option()', () => {
    it('exposes option dom element string', () => {
      const initial = 'test';
      const result = option(initial);

      expect(result).toEqual(jasmine.stringMatching('<option'));
      expect(result).toEqual(jasmine.stringMatching(`value="${initial}"`));
    });
  });
});
