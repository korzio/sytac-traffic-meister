# App

I've decided to use native JavaScript and keep the application code as simple as possible.
I prefer [TDD](https://en.wikipedia.org/wiki/Test-driven_development) approach to write code, so first I installed necessary environment.
As a test framework I've used [jasmine](https://jasmine.github.io/edge/introduction) - it has bdd framework, assertions, async possibility, spies and a small runner.
For linting my code I use eslint and [airbnb-base package](https://www.npmjs.com/package/eslint-config-airbnb-base) and a [webpack dev server](https://webpack.github.io/docs/webpack-dev-server.html) as a web server.

## Goals

- clean code
- no framework

## Notes

In first implementation I've specified all dependencies (trafficMeister and application code) explicitly in html.
After configuring webpack, I reduced external resources to single `app.js` bundle, which contains JavaScript code & styles.
To build a package there is a script specified in `package.json`

```
npm run build
# development
npm run build -- -w
```

To run tests

```
npm test
```

To start a server

```
npm start
```

Code consists of a few utilities (`intercept`, `merge`, `collect`, `capitalize`, `option`) and a bootstrap function `run`, which contains all business logic.
For the second required version I've added a `Filter`, which turns on necessary logic.

## Result

### First version

In the first version I created a draft version of an application, which behaves according requirements
![result1](./assets/app.png "First version")

### SVG version

`index.html` internally loads svg file from assets. It contains simple styles written in `css`, which set form position according to given svg file.

![result2](./assets/app2.png "SVG version")
![result3](./assets/app3.png "SVG version with filter")

### Material vesion

In material version I've choosed to use [materializecss](http://materializecss.com/) for basic html positioning.
I've used [basic demo](http://materializecss.com/templates/starter-template/preview.html) as a template view.
`index.scss` defines a subset of initial library:
- grid for positioning and responsiveness
- forms for styling checkbox and selects

I've also included `font-awesome` for icons purpose.

![result4](./assets/app4.png "Material version")

## Conclusion

Possible further optimizations:

- Use [Sets](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Set) instead of merge, intercept
- Add promisify utility to modernize a service
