const run = require('./index');

describe('index', () => {
  const formStub = {
    querySelector: () => ({}),
    addEventListener: () => {},
  };

  describe('run()', () => {
    it('starts application', () => {
      run(formStub);
    });
  });
});
