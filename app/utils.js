/* eslint-disable no-bitwise, no-confusing-arrow */
/**
 * @function
 * @name intercept
 *
 * @description
 * Check if instance presents in array and returns new array instance
 *
 * @param {array} arr
 * @param {any} instance
 * @returns {array} result
 */
// TODO rename intercept
const intercept = (arr, instance) =>
  !~arr.indexOf(instance) ? [instance] : [];
/* eslint-enable no-bitwise, no-confusing-arrow */

/**
 * @function
 * @name merge
 * @description
 * Merge two arrays into one with unique items
 * Second array can be any type, it is transformed explicitly to array
 *
 * @requires intercept
 *
 * @param {array} arr1
 * @param {array/any} arr2
 * @returns {array} result
 */
const merge = (arr1, arr2) =>
  [].concat(arr2)
    .map(instance => intercept(arr1, instance))
    .reduce((memo, interception) => memo.concat(interception), arr1);

/**
 * @function
 * @name collect
 * @description
 * Aggregates types, brands, colors from data array
 *
 * @requires merge
 *
 * @param {array[Traffic]} traffic
 * @param {object} filters
 * @returns {AggregatedTraffic} result
 */
/**
 * @typedef Traffic
 * @property {string} type
 * @property {string} brand
 * @property {array} colors
 */
/**
 * @typedef AggregatedTraffic
 * @property {array<string>?} type
 * @property {array<string>?} brand
 * @property {array<string>?} colors
 */
const collect = (traffic, filters) => {
  const values = {
    type: [],
    brand: [],
    colors: [],
  };

  const filtersKeys = Object.keys(filters || {});
  filtersKeys.forEach(key => delete values[key]);
  const valueKeys = Object.keys(values);

  traffic
    .filter(
      instance => filtersKeys.reduce((memo, key) =>
        memo && !intercept(instance[key], filters[key]).length
      , true)
    )
    .forEach(instance =>
      valueKeys.forEach((key) => {
        values[key] = merge(values[key], instance[key]);
      })
    );

  return values;
};

/**
 * @function
 * @name find
 * @description
 * Find a traffic instance by given filters
 *
 * @param {array[Traffic]} traffic
 * @param {object} filters
 * @returns {Traffic} result
 */
const find = (traffic, filters) => {
  const filtersKeys = Object.keys(filters || {})
    .filter(key => typeof filters[key] !== 'undefined');

  return traffic
    .find(
      instance => filtersKeys.reduce((memo, key) =>
        memo && !intercept(instance[key], filters[key]).length
      , true)
    );
};

/**
 * @function
 * @name capitalize
 * @description
 * Transforms string to camelCase
 *
 * @param {string} text
 * @returns {string} result
 */
const capitalize = text =>
  `${text && text.charAt(0).toUpperCase() + text.slice(1)}`;

/**
 * @function
 * @name option
 * @description
 * Creates option element string
 *
 * @requires createElement
 *
 * @param {string} value
 * @returns {string} option
 */
const option = value =>
  `<option value="${value}">${capitalize(value)}</option>`;

/**
 * @function
 * @name getFormValue
 *
 * @returns {object} selected
 */
/* global FormData */
/* eslint-disable no-restricted-syntax */
const getFormValue = (form) => {
  const formData = new FormData(form);
  const selected = {};

  for (const key of formData.keys()) {
    selected[key] = formData.get(key);
  }

  return selected;
};
/* eslint-enable no-restricted-syntax */

module.exports = {
  merge,
  intercept,
  collect,
  find,
  capitalize,
  option,
  getFormValue,
};
